/*
*   Nodo que recibirá velocidades lineales y angulares y realizara un 
*   control desacoplado por simpleza a través de un PID.
*
*/

#include "ros/ros.h"


#include "geometry_msgs/Twist.h"

#include "geometry_msgs/Vector3.h"
#include "std_msgs/Float64"

#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <sstream>

//global variables
geometry_msgs::Twist latestVel;
geometry_msgs::Vector3 control;//Angle Control Actions
std_msgs::Float64 Altitude;//Altitude Control Action

void dataCallback(const geometry_msgs::Twist::ConstPtr & message)
{
  latestVel = *message;
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "Control");

  
  ros::NodeHandle n;
  ros::Publisher ctrl_pub = n.advertise<geometry_msgs::Vector3>("copter_model/AngleControl", 100);
  ros::Publisher Alt_ctrl_pub = n.advertise<geometry_msgs::Vector3>("copter_model/AngleControl", 100);
  ros::Subscriber sub = n.subscribe("copter_model/Vel", 1000, dataCallback);
  ROS_INFO("Node: 'Control' ready");
  ros::Rate loop_rate(10);
  float K=1;
  //References:
  float RefVelx=10;
  float RefVely=10;
   while (ros::ok())
	{
  //calculamos los parametros del control:
  /* Desacoplamos el control y hacemos un control P para cada accion de control
  *  Control en velocidad: cada angulo del Swash-plate sera proporcional al error de la velocidad en x o y
  *                        La velocidad angular de los rotores sera igual para ambos pero en funcion del error con
  *                        la altura.
  */
  control.x=K*(RefVelx-latestVel.lineal.x);
  control.y=K*(RefVely-latestVel.lineal.y);
  control.z=0;

  ctrl_pub.publish(control);
  Alt_ctrl_pub.publish(Altitude);
  ros::spinOnce();

	loop_rate.sleep();
}

  return 0;
}